const Item = require("../models/Products.js")

// Create new product (admin only)
module.exports.addProduct = async (data) =>{

	if(data.isAdmin){
		let newProduct = new Item({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return await newProduct.save().then((product, error) => {

			if(error) {

				return false;
			} else {

				return true
			};
		});

	} else {
		return false;
	};
}

// Get all Active Item

module.exports.getAllActiveItems = () => {

	return Item.find({isActive: true}).then(result => {
		return result
	})
};

// Retrieve single product
module.exports.getItem = (reqParams) => {

	return Item.findById(reqParams.itemId).then(result => {
		return result
	})
};


// Update Item (admin only)
module.exports.updateItem = async (reqParams, reqBody, data) => {
	if(data.isAdmin){
		let updatedItem = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};

		return await Item.findByIdAndUpdate(reqParams.itemId, updatedItem).then((item, error) =>{

			if (error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return false
	}
}


//Archive product (admin only)
module.exports.archiveItem = async (reqParams, reqBody, data) => {
	if(data.isAdmin){
		let archivedItem = {
			isActive: false
		};

		return await Item.findByIdAndUpdate(reqParams.itemId, archivedItem).then((item, error) =>{

			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return false
	}
}