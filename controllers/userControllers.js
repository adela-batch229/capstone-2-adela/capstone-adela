const User = require("../models/Users.js");

const bcrypt = require("bcrypt")
const auth = require("../auth.js")

// User Registration
module.exports.registerUser = (reqBody) => {
	// if user exists, register fail
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return false;
		// if user does not exist, let user register
		} else {

			let newUser = new User({
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password,10)
			})

			return newUser.save().then((user,error) => {

				if(error){
					return false;
				} else {
					return true;
				}
			})
		}
	})
}

// User Authentication
module.exports.login = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access:auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

// Get User profile

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		console.log(result);

		result.password = "";

		return result;
	})
}



/*User Promotion*/
module.exports.setAdmin = async (reqParams, reqBody, data) => {
	if(data.isAdmin){
		let updatedUser = {
			isAdmin: reqBody.isAdmin
		}
		return await User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return false;
	}
}