const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers.js")
const auth = require ("../auth.js")

// Order Checkout
router.post("/createOrder", auth.verify, (req, res) => {
	const data = {
		user: auth.decode(req.headers.authorization).id,
		order: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	orderControllers.createOrder(data).then(resultFromController => res.send (resultFromController));
})

/*Get user's orders*/
router.get("/getOrders/", auth.verify, (req,res) => {
	const data = {
		user: auth.decode(req.headers.authorization).id,
	}

	orderControllers.getOrders(data).then(resultFromController => res.send(resultFromController));
})

/*Get all orders (admin only)*/
router.get("/getAllOrders", auth.verify, (req,res) => {
	const data ={
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	orderControllers.getAllOrders(data).then(resultFromController => res.send(resultFromController))
})
module.exports = router;