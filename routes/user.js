const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers.js")
const auth = require ("../auth.js")

// User registration route
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication route
router.post("/login", (req, res) => {
	userControllers.login(req.body).then(resultFromController => res.send(resultFromController));
});

//Retrieve user details
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userControllers.getProfile({userId: req.body.id}).then(
		resultFromController => res.send(resultFromController))
})


/* User Promotion*/
router.put("/:userId/setAdmin", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userControllers.setAdmin(req.params, req.body, data).then(resultFromController => res.send(resultFromController))
});


module.exports = router;